﻿using GisRocksDb.AppConfig;
using Microsoft.Extensions.Configuration;
using RocksDbSharp;
using System;
using System.Text;

namespace GisRocksDb
{
    public class RocksDB
    {
        //private readonly IConfiguration _config;

        public RocksDB()
        {
            try
            {
                AppConfiguration appConf = new AppConfiguration();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }


    private static void GarpTest()
    {
        string directory = @"C:\temp\sbys-rocksdb\0003\0003";
        DbOptions options = new DbOptions().SetCreateIfMissing(true);
        ColumnFamilies cf = new ColumnFamilies();
        ColumnFamilies cf2 = new ColumnFamilies();
        ColumnFamilyOptions cfo = new ColumnFamilyOptions();

        cf.Add("KUN.DAT.2", cfo);
        cf2.Add("KUN.DAT.2", cfo);
        cf.Add("LON.DAT.1", cfo);
        cf.Add("LON.IDX1.1", cfo);
        cf.Add("KTU.DAT.1", cfo);
        cf.Add("KTU.IDX1.1", cfo);
        cf.Add("TA.DAT.2", cfo);
        cf.Add("TA.IDX1.2", cfo);
        cf.Add("ARK.DAT.2", cfo);
        cf.Add("ARK.IDX1.2", cfo);
        cf.Add("ARK.IDX2.2", cfo);
        cf.Add("KUN.IDX1.2", cfo);
        cf.Add("ART.DAT.2", cfo);
        cf.Add("ART.IDX1.2", cfo);
        cf.Add("STR.DAT.2", cfo);
        cf.Add("STR.IDX1.2", cfo);
        cf.Add("PRD.DAT.2", cfo);
        cf.Add("PRD.IDX1.2", cfo);
        cf.Add("ORD.DAT.2", cfo);
        cf.Add("ORD.IDX1.2", cfo);
        cf.Add("ORD.IDX2.2", cfo);
        cf.Add("RSK.DAT.2", cfo);
        cf.Add("RSK.IDX1.2", cfo);
        cf.Add("RSK.IDX3.2", cfo);
        cf.Add("KTO.DAT.2", cfo);
        cf.Add("KTO.IDX1.2", cfo);
        cf.Add("SLD.DAT.2", cfo);
        cf.Add("SLD.IDX2.2", cfo);
        cf.Add("SLD.IDX3.2", cfo);
        cf.Add("BUS.DAT.2", cfo);
        cf.Add("BUS.IDX1.2", cfo);
        cf.Add("HIS.DAT.2", cfo);
        cf.Add("HIS.IDX1.2", cfo);
        cf.Add("HIS.IDX2.2", cfo);
        cf.Add("PER.DAT.2", cfo);
        cf.Add("PER.IDX1.2", cfo);
        cf.Add("TAK.DAT.2", cfo);
        cf.Add("TAK.IDX1.2", cfo);
        cf.Add("RSK.IDX2.2", cfo);
        cf.Add("SLD.IDX1.2", cfo);

        using (RocksDb rocksDb = RocksDb.Open(options, directory, cf))
        {
            string[] mget = new string[] { "A0001", "1022-4" };
            var get = rocksDb.Get("\x00\x00\x00\x0b");
            var multiget = rocksDb.MultiGet(mget);
            var a = rocksDb.GetColumnFamily("KUN.DAT.2");
            var b = rocksDb.Get("1007", a, null, null);
            var iterator = rocksDb.NewIterator(a);
            iterator.SeekToFirst();
            while (iterator.Valid())
            {
                string key = iterator.StringKey();
                var value = iterator.StringValue();
                var v2 = iterator.Value();
                string v1 = Convert.ToString(value);
                var v3 = System.Text.Encoding.Default.GetString(v2);
                var enc = Encoding.GetEncoding("iso-8859-1");
                var encodedString = enc.GetString(v2);
                Encoding iso = Encoding.GetEncoding("IBM865");
                Encoding utf8 = Encoding.UTF8;
                string res = Encoding.UTF8.GetString(v2);
                string[] lista = value.Split('\0');
                iterator.Next();
            }
        }
    }
}



