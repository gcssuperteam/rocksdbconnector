﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class Record
    {
        public Record()
        {
            Fields = new List<FieldValue>();
        }
        public List<FieldValue> Fields { get; set; }
    }
}
