﻿using GisRocksDb.AppConfig;
using GisRocksDb.DataObjects;
using GisRocksDb.Transformers;
using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace GisRocksDb.Worker
{
    public class TableReader
    {
        RocksDb _rocksDb;
        string _CurrentColumnFamily = "";
        string _DbPath = "";
        string _Table = ""; 

        public TableReader(string db_path)
        {
            try
            {
                _DbPath = db_path;
            }
            catch(Exception e)
            {

            }
        }

        public void OpenTable(string table)
        {
            _Table = table;
            _CurrentColumnFamily = FamilyDefinition.GetFamilyByTable(table);
            DbOptions options = new DbOptions().SetCreateIfMissing(true);
            
            ColumnFamilyOptions cfo = new ColumnFamilyOptions();

            _rocksDb = RocksDb.OpenReadOnly(options, _DbPath, FamilyDefinition.GetColumnFamiles(), false);
        }

        public Table ReadAllRows()
        {
            Table result = DefinitionToTableTransformer.GetTable(_Table);

            try
            {
                var a = _rocksDb.GetColumnFamily(_CurrentColumnFamily);

                var iterator = _rocksDb.NewIterator(a);
                iterator.SeekToFirst();

                // Read configuration post
                iterator.Next();

                while (iterator.Valid())
                {
                    var bvalue = iterator.Value();
                    
                    Record record = DataTransformer.DataRow(bvalue, result);

                    if(record.Fields.Count > 0)
                    {
                        result.Records.Add(record);
                    }
                    
                    iterator.Next();
                }
                
            }
            catch(Exception e)
            {

            }

            return result;
        }

 
    }
}
