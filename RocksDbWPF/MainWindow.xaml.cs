﻿using GisRocksDb;
using GisRocksDb.DataObjects;
using GisRocksDb.Worker;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RocksDbWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filepath = "";
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {

            RocksDB rb = new RocksDB();

            DateTime mTime = DateTime.Now;
            mTime = DateTime.Now;


            TableReader aga = new TableReader(@"C:\Garp\DrDenim\000");
            aga.OpenTable(txtTable.Text);
            GisRocksDb.DataObjects.Table table = aga.ReadAllRows();

            labResult.Content = "Readed " + table.Records.Count + " rows in " + DateTime.Now.Subtract(mTime).ToString();

            filepath = @"c:\temp\rocksdb_out_" + System.IO.Path.GetRandomFileName() + ".json";
            File.WriteAllText(filepath, JsonConvert.SerializeObject(table));
            

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Process myProcess = new Process();
            Process.Start(@"C:\Program Files\Notepad++\notepad++.exe", filepath);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DateTime mTime = DateTime.Now;
            mTime = DateTime.Now;

            DatasetReader aga = new DatasetReader(@"C:\Garp\Data\O310\310");
            //DatasetReader aga = new DatasetReader(@"C:\RocksData\Ebeco\000");
            aga.OpenTable(txtTable.Text);

            aga.First();

            List<Record> result = new List<Record>();
            while(aga.Valid)
            {
                result.Add(aga.GetRecord());
                aga.Next();
            }

            labResult.Content = "Readed " + result.Count + " rows in " + DateTime.Now.Subtract(mTime).ToString();

            filepath = @"c:\temp\rocksdb_out_" + System.IO.Path.GetRandomFileName() + ".json";
            File.WriteAllText(filepath, JsonConvert.SerializeObject(result));

            aga.CloseDatabase();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DateTime mTime = DateTime.Now;
            mTime = DateTime.Now;

            DatasetReader oga = new DatasetReader(@"C:\RocksData\Thermotech\000");
            //DatasetReader aga = new DatasetReader(@"C:\RocksData\Ebeco\000");
            oga.OpenTable(txtTable.Text);
            oga.SetFilter("PLF==P;LEF!=5");

            oga.First();

            List<Record> result = new List<Record>();
            while (oga.Valid)
            {
                result.Add(oga.GetRecord());
                oga.Next();
            }

            labResult.Content = "Readed " + result.Count + " rows in " + DateTime.Now.Subtract(mTime).ToString();

            //filepath = @"c:\temp\rocksdb_out_" + System.IO.Path.GetRandomFileName() + ".json";
            //File.WriteAllText(filepath, JsonConvert.SerializeObject(result));

            oga.CloseDatabase();
        }
    }
}
