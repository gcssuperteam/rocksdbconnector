﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class FieldValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
