﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GisRocksDb.AppConfig
{
    public class AppConfiguration
    {
        public readonly string _connectionString = string.Empty;

        private static List<FamilyDefinition> _familyList = new List<FamilyDefinition>();
        public static List<FamilyDefinition> FamilyList
        { 
            get
            {
                return _familyList;
            }
        }

        public AppConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder();
            
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            
            var root = configurationBuilder.Build();


            //List<string> familyList = new List<string>();
            //FamilyList = root.GetSection("FamilyList").GetChildren().Select(x => x.Value).ToList();

            _familyList.Clear();
            root.GetSection("FamilyList").Bind(_familyList);

            _connectionString = root.GetSection("FamilyList").Value;
            var appSetting = root.GetSection("ApplicationSettings");
        }

        public string ConnectionString
        {
            get => _connectionString;
        }
    }

}
