﻿using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GisRocksDb.AppConfig
{
    public class FamilyDefinition
    {
        public string Family { get; set; }
        public string Table { get; set; }



        public static ColumnFamilies GetColumnFamiles()
        {
            ColumnFamilies cf = new ColumnFamilies();
            ColumnFamilyOptions cfo = new ColumnFamilyOptions();

            
            foreach (FamilyDefinition family in AppConfiguration.FamilyList)
            {
                cf.Add(family.Family, cfo);
            }

            //cf.Add("KUN.DAT.2", cfo);
            //cf.Add("LON.DAT.1", cfo);
            //cf.Add("LON.IDX1.1", cfo);
            //cf.Add("KTU.DAT.1", cfo);
            //cf.Add("KTU.IDX1.1", cfo);
            ////cf.Add("TA.DAT.1", cfo);
            ////cf.Add("TA.IDX1.1", cfo);
            //cf.Add("ARK.DAT.2", cfo);
            //cf.Add("ARK.IDX1.2", cfo);
            //cf.Add("ARK.IDX2.2", cfo);
            //cf.Add("KUN.IDX1.2", cfo);
            //cf.Add("ART.DAT.2", cfo);
            //cf.Add("ART.IDX1.2", cfo);
            //cf.Add("STR.DAT.2", cfo);
            //cf.Add("STR.IDX1.2", cfo);
            //cf.Add("PRD.DAT.2", cfo);
            //cf.Add("PRD.IDX1.2", cfo);
            //cf.Add("ORD.DAT.2", cfo);
            //cf.Add("ORD.IDX1.2", cfo);
            //cf.Add("ORD.IDX2.2", cfo);
            //cf.Add("RSK.DAT.2", cfo);
            //cf.Add("RSK.IDX1.2", cfo);
            //cf.Add("RSK.IDX3.2", cfo);
            ////cf.Add("KTO.DAT.2", cfo);
            ////cf.Add("KTO.IDX1.2", cfo);
            //cf.Add("SLD.DAT.2", cfo);
            //cf.Add("SLD.IDX2.2", cfo);
            //cf.Add("SLD.IDX3.2", cfo);
            //cf.Add("BUS.DAT.2", cfo);
            //cf.Add("BUS.IDX1.2", cfo);
            //cf.Add("HIS.DAT.2", cfo);
            //cf.Add("HIS.IDX1.2", cfo);
            //cf.Add("HIS.IDX2.2", cfo);
            //cf.Add("PER.DAT.2", cfo);
            //cf.Add("PER.IDX1.2", cfo);
            //cf.Add("TAK.DAT.2", cfo);
            //cf.Add("TAK.IDX1.2", cfo);
            //cf.Add("RSK.IDX2.2", cfo);
            //cf.Add("SLD.IDX1.2", cfo);

            return cf;
        }

        public static string GetFamilyByTable(string table)
        {
            string result = "";

            try
            {
                result = AppConfiguration.FamilyList.Where(f => f.Table == table).FirstOrDefault().Family;
            }
            catch (Exception)
            {
            }

            return result;
        }
    }
}
