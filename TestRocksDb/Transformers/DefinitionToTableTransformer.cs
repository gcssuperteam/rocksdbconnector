﻿using GisRocksDb.DataObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GisRocksDb.Transformers
{
    public static class DefinitionToTableTransformer
    {
        public static Table GetTable(string tablename)
        {
            Table table = new Table();
            table.FieldList = new List<Field>();

            string drd = File.ReadAllText(Path.Combine(new string[] { AppDomain.CurrentDomain.BaseDirectory, "DataRowDefinitions", tablename.ToLower() + "_drd.json" }));

            table = JsonSerializer.Deserialize<Table>(drd);


            return table;
        }

    }
}
