﻿using GisRocksDb.AppConfig;
using GisRocksDb.DataObjects;
using GisRocksDb.Transformers;
using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.Worker
{
    public class DatasetReader
    {
        RocksDb _rocksDb;
        Iterator _Iterator;
        ColumnFamilyHandle _ColumnFamilyHandler;
        Table _GarpTableDefinition;

        string _CurrentColumnFamily = "";
        string _DbPath = "";

        public DatasetReader(string db_path)
        {
            try
            {
                _DbPath = db_path;
            }
            catch (Exception e)
            {

            }
        }

        public void OpenTable(string table)
        {
            _GarpTableDefinition = DefinitionToTableTransformer.GetTable(table);

            // Gets column family from translation from Garps short Table names
            _CurrentColumnFamily = FamilyDefinition.GetFamilyByTable(table);

            // Options that we can set on the RocksDb database
            DbOptions options = new DbOptions().SetCreateIfMissing(true);
            
            // We opens the database as readonly

            try
            {
                if(_rocksDb == null)
                {
                    _rocksDb = RocksDb.OpenReadOnly(options, _DbPath, FamilyDefinition.GetColumnFamiles(), false);
                    _ColumnFamilyHandler = _rocksDb.GetColumnFamily(_CurrentColumnFamily);
                    _Iterator = _rocksDb.NewIterator(_ColumnFamilyHandler);
                }
                else
                {
                    _Iterator = _rocksDb.NewIterator(_ColumnFamilyHandler);
                }
            }
            catch { }

            // Initialize iterator with columnfamily

        }

        public void CloseDatabase()
        {
            //_Iterator.Detach();
            //_rocksDb.Dispose();
            //_rocksDb = null;
        }


        public bool First()
        {
            bool result = false;

            try
            {
                // Go to first position in database
                _Iterator.SeekToFirst();
                
                // Read configuration post
                _Iterator.Next();
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public bool Last()
        {
            bool result = false;

            try
            {
                _Iterator.SeekToLast();
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public bool Next()
        {
            bool result = false;

            try
            {
                _Iterator.Next();
            }
            catch (Exception)
            {
            }

            return result;
        }

        public bool Previous()
        {
            bool result = false;

            try
            {
                _Iterator.Prev();
            }
            catch (Exception)
            {
            }

            return result;
        }

        public bool Valid 
        { 
            get
            {
                return _Iterator.Valid();
            }
        }

        public Record GetRecord()
        {
            Record result = null;
            try
            {
                // Read values and transform to human redeble things...
                result = DataTransformer.DataRow(_Iterator.Value(), _GarpTableDefinition);
            }
            catch (Exception e)
            {

            }

            return result;
        }
    }
}
