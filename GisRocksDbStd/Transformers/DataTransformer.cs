﻿using GisRocksDb.BoringThings;
using GisRocksDb.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GisRocksDb.Transformers
{
    public static class DataTransformer
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Record DataRow(byte[] brow, Table table)
        {
            Record result = new Record();

            try
            {
                int currentpos = 0;
                Int16 version = 0;
                int tableid = 0;
                byte[] shortValue = new byte[2];
                byte[] value = null;

                // Read version
                version = getShort(brow, currentpos);
                currentpos += 2;

                // Read table id
                tableid = getShort(brow, currentpos);
                currentpos += 2;

                //Check if we are on the right Table id
                if (table.TableId == tableid)
                {
                    while (currentpos < brow.Length)
                    {
                        try
                        {
                            Fieldvalue field = new Fieldvalue();

                            // Get Pos (Pos i the fields ID in Garp)
                            short pos = getShort(brow, currentpos);

                            // From Pos we get the name and the type of the field from Table.FieldList translation
                            field.Name = table.FieldList.Where(f => f.Pos == pos).FirstOrDefault()?.Name;
                            field.Type = table.FieldList.Where(f => f.Pos == pos).FirstOrDefault()?.Type;

                            currentpos += 2;

                            // Get the legth of the current field
                            short fieldLength = getShort(brow, currentpos);
                            currentpos += 2;

                            // Read value
                            value = new byte[fieldLength];
                            value = getChunck(brow, currentpos, fieldLength); //Encoding.UTF8.GetBytes(row.Substring(currentpos, value.Length));
                            currentpos += value.Length;

                            field.Value = GetValue(field.Type, value);
                            result.Fields.Add(field.Name, field);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    CorrectDecimals(result, table);
                }
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.DataRow()", e);
            }

            return result;
        }

        public static bool CheckTableId(byte[] data, short wantedtableid)
        {
            bool result = false;

            try
            {
                // Read version
                var version = getShort(data, 0);

                // Read table id
                var tableid = getShort(data, 2);


                //Check if we are on the right Table id
                if (tableid == wantedtableid)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.CorrectDecimals()", e);
            }

            return result;
        }

        public static byte[] getChunck(byte[] value, int start, int length)
        {
            byte[] result = null;

            try
            {
                result = new byte[length];
                int pos = 0;

                for (int i = start; pos < length; i++)
                {
                    result[pos++] = value[i];
                }
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.getChunck()", e);
            }

            return result;
        }

        public static Int16 getShort(byte[] value, int start)
        {
            Int16 result = 0;

            try
            {
                byte b2 = value[start];
                byte b1 = value[++start];

                result = (Int16)(b1 << 8 | b2);
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.getShort()", e);
            }

            return result;
        }

        //public static Int32 getIntSwitch(byte[] value, int start)
        //{
        //    Int32 result = 0;

        //    try
        //    {
        //        byte b1 = value[start];
        //        byte b2 = value[++start];
        //        byte b3 = value[++start];
        //        byte b4 = value[++start];

        //        result = (Int32) b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);

        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("DataTransformer.CorrectDecimals()", e);
        //    }

        //    return result;
        //}

        public static double getDouble(byte[] value, int start)
        {
            double result = 0;

            try
            {
                result = BitConverter.ToDouble(value, start);
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.getDouble()", e);
            }

            return result;
        }


        private static string GetValue(string type, byte[] value)
        {
            string result = "";

            try
            {
                if (type == "F")
                {
                    result = getDouble(value, 0).ToString(); // getInt(value, 0).ToString();
                }
                else if (type == "B")
                {
                    result = value[0].ToString();
                }
                else if (type == "C" || type == "D")
                {
                    result = GeneralFunctions.getStrFromBytes(value);
                }
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.GetValue()", e);
            }

            return result;
        }

        private static void CorrectDecimals(Record record, Table table)
        {
            try
            {
                foreach (KeyValuePair<string, Fieldvalue> fv in record.Fields)
                {
                    Fieldvalue fieldValue = fv.Value;

                    if (string.IsNullOrEmpty(fieldValue.Name))
                    {
                        continue;
                    }

                    // Get current fields definition
                    Field definition = table.FieldList.Where(f => f.Name == fieldValue.Name).FirstOrDefault();

                    // If this field is a decimal value or not (if DecimalCountPos is greater than 0 then it ponits to a ADE or PDE field)
                    if (definition.DecimalCountPos > 0)
                    {
                        // Get FieldDefinition that DecimalCountPos pointing at
                        Field definitionDecimal = table.FieldList.Where(f => f.Pos == definition.DecimalCountPos).FirstOrDefault();

                        // Get the value from FieldValue corresponding to DecimalCountPos Field

                        int decimalValue = 0;
                        if (record.Fields.ContainsKey(definitionDecimal.Name))
                        {
                            decimalValue = GeneralFunctions.getIntFromStr(record.Fields[definitionDecimal.Name].Value);
                        }

                        double newValue = GeneralFunctions.getDoubleFromStr(fieldValue.Value) / Math.Pow(10, decimalValue);
                        fieldValue.Value = GeneralFunctions.getStrFromDouble(newValue);
                    }
                    else if (definition.DecimalCountFixed > 0)
                    {
                        // Get the value from FieldValue corresponding to DecimalCountPos Field
                        double decimalValue = (double)definition.DecimalCountFixed;

                        double newValue = GeneralFunctions.getDoubleFromStr(fieldValue.Value) / Math.Pow(10, decimalValue);
                        fieldValue.Value = GeneralFunctions.getStrFromDouble(newValue);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("DataTransformer.CorrectDecimals()", e);
            }
        }
    }
}
