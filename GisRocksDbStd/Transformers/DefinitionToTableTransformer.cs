﻿using GisRocksDb.DataObjects;
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft;
using Newtonsoft.Json;

namespace GisRocksDb.Transformers
{
    public static class DefinitionToTableTransformer
    {
        public static Table GetTable(string tablename)
        {
            Table table = new Table();

            try
            {
                table.FieldList = new List<Field>();
                string drd = File.ReadAllText(Path.Combine(new string[] { AppDomain.CurrentDomain.BaseDirectory, "DataRowDefinitions", tablename.ToLower() + "_drd.json" }));
                table = JsonConvert.DeserializeObject<Table>(drd);
            }
            catch(Exception e)
            {
                Console.Write(e.Message);
            }

            return table;
        }

    }
}
