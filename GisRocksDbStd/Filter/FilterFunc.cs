﻿using GisRocksDb.BoringThings;
using GisRocksDb.DataObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDbStd.Filter
{
    public static class FilterFunc
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static FilterHolder getFilterHolder(string filter)
        {
            FilterHolder result = new FilterHolder();

            string[] sFilter = filter.Split(';');
            List<McTableFilter> lst = new List<McTableFilter>();

            // * eaquals no filter and we are returning null
            if (filter.Equals("*"))
                return null;

            foreach (string flt in sFilter)
            {
                try
                {
                    // OR filter
                    if (flt.Contains("||"))
                    {
                        string[] sOr = flt.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                        ORFilter and = new ORFilter();

                        foreach (string expression in sOr)
                        {
                            string pattern = @"(==)|(!=)|(>=)|(<=)|(!\|\^\|)|(!\^\|)|(!\|\^)|(\|\^\|)|(\^\|)|(\|\^)|(>)|(<)";
                            var s = System.Text.RegularExpressions.Regex.Split(expression, pattern);

                            if (s.Length == 3)
                            {
                                and.Filter.Add(new McTableFilter() { Field = s[0], Operand = s[1], Value = s[2] });
                            }
                        }

                        result.Or.Add(and);
                    }
                    else // AND filter
                    {
                        string pattern = @"(==)|(!=)|(>=)|(<=)|(!\|\^\|)|(!\^\|)|(!\|\^)|(\|\^\|)|(\^\|)|(\|\^)|(>)|(<)";
                        var s = System.Text.RegularExpressions.Regex.Split(flt, pattern);

                        if (s.Length == 3)
                        {
                            result.And.Filter.Add(new McTableFilter() { Field = s[0], Operand = s[1], Value = s[2] });
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error adding filter", e);
                }
            }

            return result;
        }

        internal static List<McTableFilter> getFilterList(string filter)
        {
            string[] sFilter = filter.Split(';');
            List<McTableFilter> lst = new List<McTableFilter>();

            // * eaquals no filter and we are returning null
            if (filter.Equals("*"))
                return null;

            foreach (string flt in sFilter)
            {
                try
                {
                    //string pattern = @"(==)|(!=)|(>=)|(<=)|(!\|\^\|)|(!\^\|)|(!\|\^)|(\|\^\|)|(\^\|)|(\|\^)";
                    string pattern = @"(==)|(!=)|(>=)|(<=)|(!\|\^\|)|(!\^\|)|(!\|\^)|(\|\^\|)|(\^\|)|(\|\^)|(>)|(<)";
                    var s = System.Text.RegularExpressions.Regex.Split(flt, pattern);

                    //string[] s = flt.Split(new string[] { "==", "!=", ">=", "<=", "|^", "^|", "|^|", "!|^", "!^|", "!|^|" }, 3, StringSplitOptions.RemoveEmptyEntries);

                    if (s.Length == 3)
                    {
                        lst.Add(new McTableFilter() { Field = s[0], Operand = s[1], Value = s[2] });

                        logger.Debug("Added filter Field: " + s[0] + s[1] + s[2]);
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error adding filter", e);
                }
            }

            return lst;
        }



        //internal static bool checkFilter(ref Record table, List<McTableFilter> filter)
        //{
        //    bool result = true;

        //    if (filter != null)
        //    {
        //        foreach (McTableFilter f in filter)
        //        {
        //            result = checkFilterStatement(ref table, f);

        //            if (!result)
        //            {
        //                break;
        //            }
        //        }
        //    }

        //    return result;
        //}

        internal static bool checkFilter(ref Record table, FilterHolder filter)
        {
            bool result = true;

            if (filter != null)
            {
                // First we check all AND  filter
                foreach (McTableFilter f in filter.And.Filter)
                {
                    result = checkFilterStatement(ref table, f);

                    if (!result)
                    {
                        break;
                    }
                }

                // Check all OR filters
                foreach (ORFilter and in filter.Or)
                {
                    bool orCheck = true;

                    // We check all filter statements in the OR filter, it anly takes one treu to be true
                    foreach (McTableFilter f in and.Filter)
                    {
                        orCheck = checkFilterStatement(ref table, f);

                        if (orCheck)
                        {
                            break;
                        }
                    }

                    // If we didn't get at true the current OR filters returned false and this will set the whole filter statement to FALSE
                    if (!orCheck)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        private static bool checkFilterStatement(ref Record table, McTableFilter filter)
        {
            bool result = true;
            Fieldvalue fieldValue = null;

            try
            {
                if (table.Fields.ContainsKey(filter.Field))
                {
                    fieldValue = table.Fields[filter.Field];
                }
                else
                {
                    fieldValue = new Fieldvalue { Name = filter.Field, Value = "", Type = "C" };
                }

                if (filter.Operand.Equals("!="))
                {
                    if (fieldValue.Type == "F")
                    {
                        if (GeneralFunctions.getDecimalFromStr(fieldValue.Value) != GeneralFunctions.getDecimalFromStr(filter.Value))
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {

                        if (fieldValue.Value.Trim().Equals(filter.Value))
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (filter.Operand.Equals("=="))
                {
                    if (fieldValue.Type == "F")
                    {
                        if (GeneralFunctions.getDecimalFromStr(fieldValue.Value.Trim()) == GeneralFunctions.getDecimalFromStr(filter.Value))
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        if (fieldValue.Value.Trim().Equals(filter.Value))
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (filter.Operand.Equals(">="))
                {
                    // Different compares depending on text values and numeric values
                    if (fieldValue.Type == "F")
                    {
                        if (GeneralFunctions.getDecimalFromStr(fieldValue.Value.Trim()) >= GeneralFunctions.getDecimalFromStr(filter.Value))
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        if (fieldValue.Value.Trim().CompareTo(filter.Value) >= 0)
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (filter.Operand.Equals("<="))
                {
                    // Different compares depending on text values and numeric values
                    if (fieldValue.Type == "F")
                    {
                        if (GeneralFunctions.getDecimalFromStr(fieldValue.Value.Trim()) <= GeneralFunctions.getDecimalFromStr(filter.Value))
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        if (fieldValue.Value.Trim().CompareTo(filter.Value) <= 0)
                            result = true;
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (filter.Operand.Equals("|^"))
                {
                    if (fieldValue.Value.Trim().StartsWith(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
                else if (filter.Operand.Equals("^|"))
                {
                    if (fieldValue.Value.Trim().EndsWith(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
                else if (filter.Operand.Equals("|^|"))
                {
                    if (fieldValue.Value.Trim().Contains(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
                else if (filter.Operand.Equals("!|^"))
                {
                    if (!fieldValue.Value.Trim().StartsWith(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
                else if (filter.Operand.Equals("!^|"))
                {
                    if (!fieldValue.Value.Trim().EndsWith(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
                else if (filter.Operand.Equals("!|^|")) //(!f.Operand.Equals("!|^|"))
                {
                    if (!fieldValue.Value.Trim().Contains(filter.Value))
                        result = true;
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("FilterFunc.checkFilterStatement()", e);
            }

            return result;
        }

        //internal static bool checkFilter(ref DbGarpFile table, List<McTableFilter> filter)
        //{
        //    bool result = true;

        //    if (filter != null)
        //    {
        //        foreach (McTableFilter f in filter)
        //        {
        //            if (f.Operand.Equals("!="))
        //            {
        //                if (!table.getFieldByName(f.Field).getText().Trim().Equals(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("=="))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().Equals(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals(">="))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().CompareTo(f.Value) >= 0)
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("<="))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().CompareTo(f.Value) <= 0)
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("|^"))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().StartsWith(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("^|"))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().EndsWith(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("|^|"))
        //            {
        //                if (table.getFieldByName(f.Field).getText().Trim().Contains(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("!|^"))
        //            {
        //                if (!table.getFieldByName(f.Field).getText().Trim().StartsWith(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (f.Operand.Equals("!^|"))
        //            {
        //                if (!table.getFieldByName(f.Field).getText().Trim().EndsWith(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else if (!f.Operand.Equals("!|^|"))
        //            {
        //                if (!table.getFieldByName(f.Field).getText().Trim().Contains(f.Value))
        //                    result = true;
        //                else
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }

        //        }

        //    }

        //    return result;
        //}
    }

    public class FilterHolder
    {
        public FilterHolder()
        {
            this.Or = new List<ORFilter>();
            this.And = new ANDFilter();
        }

        public ANDFilter And { get; set; }
        public List<ORFilter> Or { get; set; }
    }

    public class ANDFilter
    {
        public ANDFilter()
        {
            this.Filter = new List<McTableFilter>();
        }

        public List<McTableFilter> Filter { get; set; }
    }

    public class ORFilter
    {
        public ORFilter()
        {
            this.Filter = new List<McTableFilter>();
        }

        public List<McTableFilter> Filter { get; set; }
    }

    public class McTableFilter
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public string Operand { get; set; }
    }
}
