﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class Record
    {
        public Record()
        {
            Fields = new Dictionary<string, Fieldvalue>();
        }

        public Dictionary<string, Fieldvalue> Fields { get; set; }
    }
}
