﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class Table
    {
        public Table()
        {
            FieldList = new List<Field>();
            Records = new List<Record>();
        }

        public short TableId { get; set; }
        public string Name { get; set; }
        public List<Record> Records { get; set; }
        public List<Field> FieldList { get; set; }
    }
}
