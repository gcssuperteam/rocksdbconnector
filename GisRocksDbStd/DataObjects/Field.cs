﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class Field
    {
        public string Name { get; set; }
        public int Pos { get; set; }
        public string Type { get; set; }
        public int DecimalCountPos { get; set; }
        public int DecimalCountFixed { get; set; }
    }
}
