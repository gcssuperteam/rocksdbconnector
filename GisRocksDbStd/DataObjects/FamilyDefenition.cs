﻿using Newtonsoft.Json;
using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class FamilyList
    {
        public List<FamilyDefinition> Families { get; set; }
    }

    public class FamilyDefinition
    {
        public string Family { get; set; }
        public string Table { get; set; }
    }
}
