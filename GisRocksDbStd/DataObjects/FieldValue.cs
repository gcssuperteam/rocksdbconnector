﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GisRocksDb.DataObjects
{
    public class Fieldvalue
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
