﻿using GisRocksDb.DataObjects;
using Newtonsoft.Json;
using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GisRocksDbStd.Family
{
    public class FamilyFunc
    {
        private static List<FamilyDefinition> _familyList = new List<FamilyDefinition>();



        public static ColumnFamilies GetColumnFamiles()
        {
            ColumnFamilies cf = new ColumnFamilies();
            ColumnFamilyOptions cfo = new ColumnFamilyOptions();


            foreach (FamilyDefinition family in _familyList)
            {
                // Dont return CF more than once
                if(cf.Where(c=>c.Name == family.Family).Count() == 0)
                {
                    cf.Add(family.Family, cfo);
                }
                
            }
            return cf;
        }

        public static ColumnFamilies GetColumnFamiles(List<string> families)
        {
            ColumnFamilies cf = new ColumnFamilies();
            ColumnFamilyOptions cfo = new ColumnFamilyOptions();

            foreach (string s in families)
            {
                // Dont return CF more than once
                cf.Add(s, cfo);
            }
            return cf;
        }


        public static string GetFamilyByTable(string table)
        {
            string result = "";

            try
            {
                if(_familyList.Count == 0)
                {
                    LoadFamilies();
                }

                result = _familyList.Where(f => f.Table == table).FirstOrDefault().Family;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }

        public static List<FamilyDefinition> FamilyList
        {
            get
            {
                if (_familyList.Count == 0)
                {
                    LoadFamilies();
                }

                return _familyList;
            }
        }

        private static void LoadFamilies()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            var json = File.ReadAllText(path);

            List<FamilyDefinition> defList = JsonConvert.DeserializeObject<List<FamilyDefinition>>(json);

            _familyList.Clear();
            _familyList.AddRange(defList);

        }
        //private static void LoadFamilies()
        //{
        //    var configurationBuilder = new ConfigurationBuilder();

        //    var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
        //    configurationBuilder.AddJsonFile(path, false);

        //    var root = configurationBuilder.Build();


        //    //List<string> familyList = new List<string>();
        //    //FamilyList = root.GetSection("FamilyList").GetChildren().Select(x => x.Value).ToList();

        //    _familyList.Clear();
        //    root.GetSection("FamilyList").Bind(_familyList);

        //    _connectionString = root.GetSection("FamilyList").Value;
        //    var appSetting = root.GetSection("ApplicationSettings");
        //}
    }
}
