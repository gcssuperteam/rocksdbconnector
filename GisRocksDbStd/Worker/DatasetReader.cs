﻿using GisRocksDb.AppConfig;
using GisRocksDb.DataObjects;
using GisRocksDb.Transformers;
using GisRocksDbStd.Family;
using GisRocksDbStd.Filter;
using RocksDbSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GisRocksDb.Worker
{
    public class DatasetReader
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        RocksDb _rocksDb;
        Iterator _Iterator;
        ColumnFamilyHandle _ColumnFamilyHandler;
        
        int count = 0;
        FilterHolder _Filter = null;
        Record _CurrentReccord = null;

        string _CurrentColumnFamily = "";
        string _DbPath = "";

        
        public Table GarpTableDefinition { get; set; }

        public DatasetReader(string db_path)
        {
            try
            {
                _DbPath = db_path;
            }
            catch (Exception e)
            {

            }
        }

        public void SetFilter(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                _Filter = FilterFunc.getFilterHolder(filter);
            }
        }

        public void OpenTable(string table)
        {
            try
            {
                GarpTableDefinition = DefinitionToTableTransformer.GetTable(table);

                // Gets column family from translation from Garps short Table names
                _CurrentColumnFamily = FamilyFunc.GetFamilyByTable(table);

                // Options that we can set on the RocksDb database
                DbOptions options = new DbOptions().SetCreateIfMissing(true);

                //options.SetDbLogDir(@"c:\temp\rockslogg");
                
                if (_rocksDb == null)
                {
                    List<string> dbColumnFamilyList = RocksDb.ListColumnFamilies(options, _DbPath).ToList();
                    _rocksDb = RocksDb.OpenReadOnly(options, _DbPath, FamilyFunc.GetColumnFamiles(dbColumnFamilyList), false);

                    // Get the implicit version of column family in database (new versioen everey time a recinciliation is done)

                    _ColumnFamilyHandler = _rocksDb.GetColumnFamily(dbColumnFamilyList.Where(c=>c.StartsWith(_CurrentColumnFamily)).First());
                    _Iterator = _rocksDb.NewIterator(_ColumnFamilyHandler);
                }
                else
                {
                    _Iterator = _rocksDb.NewIterator(_ColumnFamilyHandler);
                }
                
            }
            catch(Exception e)
            {
                logger.Error("DatasetReader.OpenTable()", e);
            }
        }

        public void CloseDatabase()
        {
            //_Iterator.Detach();
            //_rocksDb.Dispose();
            //_rocksDb = null;
        }


        public bool First()
        {
            bool result = false;

            try
            {
                // Go to first position in database
                _Iterator.SeekToFirst();
                
                // Read configuration post
                _Iterator.Next();
            }
            catch (Exception e)
            {
                logger.Error("DatasetReader.First()", e);
            }

            return result;
        }

        public bool Last()
        {
            bool result = false;

            try
            {
                _Iterator.SeekToLast();
            }
            catch (Exception e)
            {
                logger.Error("DatasetReader.Last()", e);
            }

            return result;
        }


        public bool Next()
        {
            bool result = false;
            bool tableMatch = false;
            bool filterMatch = false;

            try
            {
                // Loop through alla records and select only records with same TableId as wanted, and also check if we reached end of file
                while (!tableMatch || !filterMatch) 
                {
                    _Iterator.Next();

                    if(!_Iterator.Valid())
                    {
                        break;
                    }

                    //byte[] key = _Iterator.Key();
                    
                    count++;

                    tableMatch = false;
                    filterMatch = false;

                    if(DataTransformer.CheckTableId(_Iterator.Value(), GarpTableDefinition.TableId))
                    {
                        tableMatch = true;
                        _CurrentReccord = DataTransformer.DataRow(_Iterator.Value(), GarpTableDefinition);

                        if (FilterFunc.checkFilter(ref _CurrentReccord, _Filter))
                        {
                            filterMatch = true;
                        }
                    }

                    // Check if we have more records to read
                    if (!_Iterator.Valid())
                    {
                        result = false;
                        break;
                    }

                } 
                //do
                //{
                //    count++;
                //    _Iterator.Next();
                //    if (!_Iterator.Valid())
                //    { 
                //        break;
                //    }
                    
                //} while (!DataTransformer.CheckTableId(_Iterator.Value(), _GarpTableDefinition.TableId));

            }
            catch (Exception e)
            {
                logger.Error("Error in DatasetReader.Next()", e);
            }

            return result;
        }

        public bool Previous()
        {
            bool result = false;

            try
            {
                _Iterator.Prev();
            }
            catch (Exception)
            {
            }

            return result;
        }

        public bool Valid 
        { 
            get
            {
                return _Iterator.Valid();
            }
        }

        public Record GetRecord()
        {
            Record result = null;
            try
            {
                // Read values and transform to human redeble things...
                result = DataTransformer.DataRow(_Iterator.Value(), GarpTableDefinition);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }
    }
}
